﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

using Mapbox.Examples;
using Mapbox.Unity.Map;
using Mapbox.Utils;

public class DirectorScript : MonoBehaviour {

    public GameObject mapObject;

    public GameObject guiWarning;

    public GameObject guiCrosshair;

    public GameObject guiLocate;

    public GameObject guiLocation;

    public Vector2d[] locTargets;

    public Vector2d[] locRandom;

    public float zoom = 2.5f;

    public float focusZoom = 18f;

    public float overviewZoom= 15.5f;

    private AbstractMap map;

    private Animator animator;

    private AudioSource audio;

    private SpawnOnMap markers;

    // Start is called before the first frame update
    void Start() {
        this.map = this.mapObject.GetComponent<AbstractMap>();
        this.animator = this.GetComponent<Animator>();
        this.audio = this.GetComponent<AudioSource>();
        this.markers = this.mapObject.GetComponent<SpawnOnMap>();

        SpawnOnMap som = this.map.GetComponent<SpawnOnMap>();

        if(this.guiWarning != null)
            this.guiWarning.active = false;
        if(this.guiCrosshair != null)
            this.guiCrosshair.active = false;
        if(this.guiLocate != null)
            this.guiLocate.active = false;
        if(this.guiLocation != null)
            this.guiLocation.active = false;

        this.map.UpdateMap(this.zoom);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown("space"))
            this.animator.SetTrigger("Start");
    }

    void GoRandomLocation() {
        this.map.UpdateMap(this.locRandom[Random.Range(0, this.locRandom.Length - 1)]);
    }

    void GoTargetLocation() {
        this.map.UpdateMap(this.locTargets[Random.Range(0, this.locTargets.Length - 1)]);
    }

    void FindAndZoom() {
        if(this.zoom < 18f)
            this.zoom += 1.0f;
        this.map.UpdateMap(this.locTargets[Random.Range(0, this.locTargets.Length - 1)], this.zoom);

        if(this.guiCrosshair != null)
            this.guiCrosshair.active = true;
        if(this.guiLocate != null)
            this.guiLocate.active = true;
    }

    void FocusOverview() {
        if(this.guiLocate != null)
            this.guiLocate.active = false;
        if(this.guiLocation != null)
            this.guiLocation.active = true;

        var locations = new string[this.locTargets.Length];
        for(int i = 0; i < locations.Length; i++)
            locations[i] = this.locTargets[i].x + ", " + this.locTargets[i].y;
		this.markers._locationStrings = locations;

        string locationsText = "";
        for(int i = 0; i < this.locTargets.Length; i++)
            locationsText += this.locTargets[i].x + ", " + this.locTargets[i].y + "\n";
        this.guiLocation.GetComponentInChildren<Text>().text = locationsText;

        this.map.UpdateMap(this.overviewZoom);
    }

    void FocusTarget() {
        if(this.guiLocate != null)
            this.guiLocate.active = false;

        this.map.UpdateMap(this.locTargets[Random.Range(0, this.locTargets.Length)], this.focusZoom);
    }

    void Alarm() {
        if(this.audio != null)
            this.audio.Play();
        if(this.guiWarning != null)
            this.guiWarning.active = true;
    }
}
