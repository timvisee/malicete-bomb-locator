﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class RandomText : MonoBehaviour
{
    private Text text;

    public float intervalSeconds;

    private float lastChange = -1f;

    // Start is called before the first frame update
    void Start() {
        this.text = this.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        if(this.lastChange < 0f || Time.timeSinceLevelLoad - this.lastChange > this.intervalSeconds) {
            this.lastChange = Time.timeSinceLevelLoad;
            this.text.text = GetRandomText();
        }
    }

    string GetRandomText() {
        string glyphs= "ABCDEF0123456789#####";
        string text = "";
        for(int i=0; i < 14; i++) {
            text += glyphs[Random.Range(0, glyphs.Length)];
        }
        return text;
    }
}
