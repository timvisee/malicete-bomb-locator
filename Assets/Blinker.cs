﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour {

    private Canvas canvas;

    void Start() {
        this.canvas = this.GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update() {
        this.canvas.enabled = (Time.timeSinceLevelLoad * 2.5f % 3) < 2.0f;
    }
}
